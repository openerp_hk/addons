# -*- coding: utf-8 -*-
{
    'name': "微信扫码登录",

    'summary': """
        微信登入测试程序""",

    'description': """
        
    """,

    'author': "openerp.hk",
    'website': "cdn.openerp.hk",

    # data.xml
    # for the full list
    'category': 'openerphk',
    'version': '0.1',
    'installable': True,

    # any module necessary for this one to work correctly
    'depends': ['base', 'auth_oauth', 'website'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        # 'views/views.xml',
        'views/auth_oauth_views_extend.xml',
        'views/customer_bind_user_view.xml',
        'views/error_or_thank_page.xml',
    ],
    'application': True,
    'sequence': 1,
}