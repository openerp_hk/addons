# -*- coding: utf-8 -*-
{
    'name': "quick_search_demo",

    'summary': """
        教你如何添加快速搜索""",

    'description': """
        如何进行快速搜索的教程
    """,

    'author': "openerp.hk",
    'website': "http://cdn.openerp.hk",

    # Categories can be used to filter modules in modules listing

    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['sale'],

    # always loaded
    'data': [
        'views/quotation_search.xml',
    ]
}
